#!/usr/bin/env python3

from mpi4py import MPI

from collections import defaultdict
import numpy as np

from src.IDA import * 
import sys
import csv
from multiprocessing import Pool
import time
import argparse


#just mention which graph you are using in this variable
path = "given_graphs/smallRandom.json"
rec_lim = 100000


if __name__ == "__main__" :


    parser = argparse.ArgumentParser(
                                    description='Creating a pool of compute nodes and Job(future tasks sent to a job will transmit to pool).',
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter
                                    )
    parser.add_argument('--graph_path', help='Path of the Graph', dest='path', default='given_graphs/smallRandom.json')
    parser.add_argument('--rec_lim', help='Recursion Limit', dest='rec_lim', default=100000, type = int)
    parser.add_argument('--nb_process', help='Number of process to compute tasks', dest='nb_proc', default = 5, type = int)
    args = parser.parse_args()

    path = args.path
    rec_lim = args.rec_lim
    nb_process = args.nb_proc
    print("out of main")


G = Tree(f"./data/{path}",nb_process)
sys.setrecursionlimit(100000)


# Making the calculation of the upper bound
tasks = G.tasks
sum =0
for task in tasks.values() :
    sum += task.time
percentage = 0.1
# Initializing the MPI interface and objects
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

sz = 1
double_size =  MPI.DOUBLE.Get_size()

if rank == 0:
    nbbytes = sz*double_size
else:
    nbbytes = 0

# Verifying the number of processes
if size <= 1:
    raise ValueError("At least two processes are needed for this MPI architecture")

upper_bound = sum

# Declaring the function 'search' to search for the solution
def search(node):
    sorted_children = sorted(G.children(node),key=lambda u:u.g+G.h(u))
    sorted_children = sorted_children[:max(int(len(sorted_children)*percentage),1)]
    for u in sorted_children:
        if u.g > upper_bound:
            continue
        if G.is_terminal(u):
            #upper_bound = min(upper_bound,u.g)
            return u
        return search(u)
    return None


print(f'hello from {rank} in {size}')
sorted_children = sorted(G.children(G.root),key=lambda u:u.g+G.h(u))
total_answers = []
process_answers = []



if rank == 0:
    # If the process is the mainly one (rank=0), it will wait
    # the calculations of the other processes.
    time_benchmarks = {}
    print(f'Im in!')
    start = time.perf_counter()
    time_benchmarks["after building tree"] = time.perf_counter()
    for id in range(1,size):
        total_answers.append(comm.recv(source=id))
    print(f'messages received')
        
else:

    # If the process is not the mainly one (rank!=0), it will search
    # for possible solutions of the problem.

    print(f'im out!')
    for i in range((len(sorted_children)-rank)//(size-1)+1):    
        node_answer = search(sorted_children[i*(size-1)+rank-1])
        process_answers.append(node_answer)
    comm.send(process_answers,dest=0)
    print(f'message send in {rank}')

# After receive the answers of all processes, the mainly process
# will find the best solution.

if rank==0:
    solution = None
    for process_answers in total_answers:
        for node_answer in process_answers:
            if node_answer is None:
                continue
            else:
                if solution is None:
                    solution = node_answer
                elif node_answer.g < solution.g:
                    solution = node_answer
    print("solution gives : ",solution.hist)
    time_benchmarks["end"] = time.perf_counter()

    for v in (time_benchmarks.keys()) :
        print(f'Time to reach {v}: {time_benchmarks[v] - start}')

